#!/usr/bin/env bash

start_task_group() {
    local path=${1}
    local task more_than_one

    trap '' INT

    load_rc ${GD_ENVS}/${GD_ENV}
    load_rc ${GD_EDITORS}/${GD_EDITOR}

    for task in $(list_tasks "${path}" | sort ${GD_SORT_OPTS}); do
        if [[ -v more_than_one ]]; then
            echo ""
            ask "Try next task from '${path}'?" || break
        fi

        (start_round "${task}")

        more_than_one=true
    done

    trap - INT
}

start_round() {
    local task=${1:?}

    load_rc "${GD_TASKS}/${task}"

    tmux new-window \
        -n "${task}" \
        -e GD_ENV="${GD_ENV}" \
        -e GD_EDITOR="${GD_EDITOR}" \
        -e GD_EDIT_COMMAND="${GD_EDIT_COMMAND}" \
        -e GD_MAKE_COMMAND="${GD_MAKE_COMMAND}" \
        -e GD_TEST_COMMAND="${GD_TEST_COMMAND}" \
        -e GD_TIMEOUT="${GD_TIMEOUT}" \
        -e GD_STEP="${GD_STEP}" \
        ${GD_ROOT}/lib/round.sh "${task}" "$((GD_TIMEOUT + GD_TIMEOUT_MOD))" "${GD_STEP}"
}

load_rc() {
    local path="${1:?}"
    if [[ ! -d ${path} ]]; then
        local dir=$(cd ${path%/*} && pwd)
    else
        local dir=$(cd ${path} && pwd)
    fi
   
    local -a rcs 
    while [[ ${dir} = ${GD_ROOT}* || ${dir} = ${GD_TASKS}* ]]; do
        rc="${dir}/.gdrc"
        [[ -f ${rc} ]] && rcs+=("${rc}")
        dir="${dir%/*}"     # dirname ${dir}
    done
    for ((i=$((${#rcs[*]}-1)); i>=0; i--)); do
        source ${rcs[i]}
    done
}
export -f load_rc

list_tasks() {
    local path="${1:?}"
    local dir name 

    if [[ ! -f "${GD_TASKS}/${path}.md" ]] && [[ ! -d "${GD_TASKS}/${path}" ]]; then
        die "Task or task group '${path}' does not exist!"
    elif [[ -d "${GD_TASKS}/${path}" ]]; then
        dir="${GD_TASKS}/${path}"
        name="*.md"
    else
        dir="${GD_TASKS}/${path%/*}"   # dirname ${task_path}
        name="${path##*/}"             # basename ${task_path}
    fi
    find -L ${dir} -name "${name%.md}.md" | sed -e "s|^${GD_TASKS}/||" -e 's/.md$//'
}

list_task_groups() {
    local path="${1}"

    if [[ ! -d "${GD_TASKS}/${path}" ]]; then
        die "Parent group '${path}' does not exist!"
    fi

    find -L "${GD_TASKS}/${path}" -name '*' -type d | grep -v "^${GD_TASKS}/$" | sed -e "s|^${GD_TASKS}/||"
}
