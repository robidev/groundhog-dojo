#!/usr/bin/env bash

log_stats() {
    local task="${1:?}"
    local time="${2:?}"
    local succ="${3:?}"

    echo "${task};${succ};$(ftime ${time});$(date -Iseconds);${GD_ENV};${GD_EDITOR}"
} >> ${GD_DATA}/stats

print_stats() {
    local task="${1:?}"
    echo ""
    {
        local _task _passed _time _date _env _editor
        echo "TASK;RESULT;TIME;DATE;ENV;EDITOR"
        while IFS=';' read _task _passed _time _date _env _editor _; do
            echo "${_task};$(result ${_passed});${_time};$(date -d "${_date}");${_env};${_editor}"
        done < <(grep -F "${task}" ${GD_DATA}/stats)
    } | column -t -s ';'
    echo ""
}

