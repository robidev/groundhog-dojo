#!/usr/bin/env bash

rseq() {
    if [[ $3 -eq 1 ]]; then
        echo "1"
    else
        echo -n "${3} "
        rseq $1 $2 $(($3 * $2 % $1))
    fi
}

export -f rseq
