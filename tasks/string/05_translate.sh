#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:  "" "" ""
output: ""
---
input:  "" "" "ABC"
output: "ABC"
---
input:  "ABC" "XYZ" ""
output: ""
---
input:  "A" "X" "A"
output: "X"
---
input:  "ABC" "XYZ" "DEF"
output: "DEF"
---
input:  "ABC" "XYZ" "ABC"
output: "XYZ"
---
input:  "AB" "XY" "ABABABABA"
output: "XYXYXYXYX"
---
summary
