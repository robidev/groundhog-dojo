#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:  ""
output: 0
---
input:  "foo"
output: 1
---
input:  "foo bar"
output: 2
---
input:  "foo bar baz"
output: 3
---
input:  "foo  bar"
output: "3"
---
input:  " foo bar "
output: ":::"
---
summary
