#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:  ""
output: ""
---
input:  "foo"
output: "foo"
---
input:  "foo" "bar"
output: "foo:bar"
---
input:  "foo" "bar" "baz"
output: "foo:bar:baz"
---
input:  "" ""
output: ":"
---
input:  ":" ":"
output: ":::"
---
summary
