#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:  i
output: true
---
input:  ii
output: true
---
input:  mi
output: false
---
input:  im
output: false
---
input:  mim
output: true
---
input:  madamimadam
output: true
---
input:  aibohphobia
output: true
---
summary
