#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:  
output: 
---
input:  5
output: 5
---
input:  5 4
output: 4 5
---
input:  5 4 6
output: 4 6 5
---
input:  1 {2..100}
output: {2..100} 1
---
summary
