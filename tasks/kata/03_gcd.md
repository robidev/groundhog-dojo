# Greatest Common Divisor (GCD)

Write a program which, when given two numbers as arguments, prints their greatest common divisor (GCD).

GCD of two numbers is the greatest number, by which both numbers are divisable.

Example:
```
$ run 12 8
4
```
