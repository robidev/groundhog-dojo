# Anagram

Write a program which, when given two strings as arguments, prints "true", when one is an anagram of the other, and "false" otherwise.

Anagram of a word is such a word which consists of the same letters, but in a different order.

Example:
```
$ run \
     "TOMMARVOLORIDDLE" \
     "IAMLORDVOLDEMORT"
true
```
