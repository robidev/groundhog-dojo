#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:
output:
---
input:  5
output:
---
input:  5 5
output:
---
input:  5 4
output: 4
---
input:  5 4 6 3 7 2
output: 6
---
summary
