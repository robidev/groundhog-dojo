#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:  0
output: 0
---
input:  3
output: 1
---
input:  00
output: 0
---
input:  30
output: 1
---
input:  33
output: 2
---
input:  030
output: 1
---
input:  303
output: 2
---
input:  3030303
output: 4
---
summary
