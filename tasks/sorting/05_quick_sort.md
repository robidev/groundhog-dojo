# Quick Sort

Write a program which, when given an array of integers, returns the numbers sorted ascendingly using Quick Sort algorithm.

Quick Sort works by partitioning the elements by being greater or less than a chosen element (pivot), and then (quick-)sorting the
partitions.

Example:
```
$ run 8 1 6 3 4 5 2 7
1 2 3 4 5 6 7 8
```
