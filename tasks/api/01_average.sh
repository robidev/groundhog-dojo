#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input: 
output: 
---
input:  5
output: 5.0
---
input:  5 4
output: 4.5
---
input:  5 4 6
output: 5.0
---
input:  5 4 6 3
output: 4.5
---
input:  5 4 6 3 7 2
output: 4.5
---
summary
