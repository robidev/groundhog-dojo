#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input: "0"
output: 0
---
input: "F"
output: 15
---
input: "10"
output: 16
---
input: "20"
output: 32
---
input: "B00B5"
output: 721077
---
input: "CAFEBABE"
output: 3405691582
---
input: "DEADBEEF"
output: 3735928559
---
summary
