#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input: "00:00:00"
output: true
---
input: "22:36:44"
output: true
---
input: "24:33:44"
output: false
---
input: "09:61:44"
output: false
---
input: "01:21:84"
output: false
---
summary
