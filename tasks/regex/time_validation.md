# Validate the time

Write a program which, when given a string as argument, prints `true`, when string matches the 24-hours clock (hh:mm:ss), and `false` otherwise.

Example:
```
$ run "22:36:44"
true
```
