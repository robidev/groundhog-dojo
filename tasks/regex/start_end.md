# Start and end of the matched subsequence

Write a program which, when given two strings as arguments (first pattern, second input), prints starting (inclusive) and ending (exclusive) index of the matched subsequence.

Example:
```
$ run \
> "genius" \
> "The difference between stupidity and \
>  genius is that genius has its limits."
start: 37, end: 43
start: 52, end: 58
```
