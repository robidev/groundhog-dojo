#!/usr/bin/env bash

export GD_ROOT=${GD_ROOT:-${PWD}}
export GD_WORK=${GD_WORK:-${GD_ROOT}}
export GD_DATA=${GD_DATA:-${GD_WORK}/.data}
export GD_ENVS=${GD_ENVS:-${GD_ROOT}/envs}
export GD_EDITORS=${GD_EDITORS:-${GD_ROOT}/editors}
export GD_TASKS=${GD_TASKS:-${GD_ROOT}/tasks}

source ${GD_ROOT}/lib/util.sh
source ${GD_ROOT}/lib/opts.sh
source ${GD_ROOT}/lib/stat.sh
source ${GD_ROOT}/lib/task.sh
source ${GD_ROOT}/lib/menu.sh

main() {
    setup
    if [[ -z ${1} ]]; then
        menu "$@"
    else
        while true; do
            start_task_group "$1"
            shift
            [[ -z $1 ]] && break
        done
    fi
}

setup() {
    mkdir -p ${GD_DATA}

    check_env "${GD_ENV}" || unset GD_ENV
    check_editor "${GD_EDITOR}" || unset GD_EDITOR

    export GD_EDITOR=${GD_EDITOR:-vim}
    export GD_ENV=${GD_ENV:-java}

    export GD_EDIT_COMMAND GD_MAKE_COMMAND GD_TEST_COMMAND
    export GD_TIMEOUT GD_TIMEOUT_MOD GD_STEP

    tmux rename-window Menu
    #tmux set-option status off
    tmux unbind-key C-Left
    tmux unbind-key C-Right
    tmux unbind-key C-Up
    tmux unbind-key C-Down

    cd "${GD_WORK}"
    [[ -d .git ]] || init_git_repo
}

init_git_repo() {
    git init
    git config user.name 'Groundhog Dojo'
    git config user.email 'gd@example.com'
    git add .
    git commit -m "Initial commit"
} > /dev/null

if [[ -z ${TMUX} ]]; then
    parse_opts "$@"
    tmux -L "${GD_SESSION:-gd-$$}" new-session -x - -y - "$0" "$@"
else
    parse_opts "$@"
    eval set -- "$ARGS"
    main "$@"
fi
