#!/bin/bash

export GD_ROOT="${GD_ROOT:-$(cd $(dirname "$0") && pwd)}"
export GD_WORK="${GD_WORK:-"/tmp/gd-$$"}"
export PATH=${GD_ROOT}/bin:${PATH}

[[ -d ${GD_WORK} ]] && rm -r "${GD_WORK}"
mkdir -p "${GD_WORK}"
cp -r "${GD_ROOT}/src" "${GD_ROOT}/Makefile" "${GD_WORK}"

exec ${GD_ROOT}/gd.sh "$@"
