DEPS := git tmux vim

.PHONY: all deps compile test-compile test clean

compile:
	${GD_MAKE_COMMAND} compile

test-compile:
	${GD_MAKE_COMMAND} test-compile

test: compile test-compile
	bash "${GD_TASK}.sh"

clean:
	${GD_MAKE_COMMAND} clean

deps:
	which $(DEPS) >/dev/null || { sudo apt-get update && sudo apt-get -y install $(DEPS); }

all: clean compile test-compile
