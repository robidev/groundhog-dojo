#!/usr/bin/env bash

set -e

if ! which docker; then
    echo "Docker not available. Exiting!" >&2
    exit 1
fi

OUT=/dev/stdout

if [[ ${1} == "-q" ]]; then
    OUT=/dev/null
    shift
fi

docker build -f Dockerfile -t groundhog-dojo . > ${OUT}
docker run -it --rm groundhog-dojo "${@}"
